# Gimp City

## Description
A collection of 3 dark themes for GIMP inspired by City Lights.

For more information about GIMP, visit the [GIMP.org website](https://www.gimp.org/).

## Screenshots
![GIMP City All](https://gitlab.com/perelax/src_imgs/-/raw/main/gimp/gimp-city-all-web.png)*GIMP City Dark Themes*


![GIMP City Lights](https://gitlab.com/perelax/src_imgs/-/raw/main/gimp/gimp-city-lights-web.png)


![GIMP City Nights](https://gitlab.com/perelax/src_imgs/-/raw/main/gimp/gimp-city-nights-web.png)


![GIMP City Black](https://gitlab.com/perelax/src_imgs/-/raw/main/gimp/gimp-city-black-web.png)

## Installation
Ubuntu:

Open a terminal in the theme download directory and extract theme file to the GIMP theme directory using the appropriate command below.

Alternatively, open the GIMP themes directory as root, and simply copy/paste the GIMP City theme folder(s) into it.

Gimp City All Themes
```
sudo tar xfC gimp-city-all.tar.gz /usr/share/gimp/2.0/themes

```
Gimp City Lights
```
sudo tar xfC gimp-city-lights.tar.gz /usr/share/gimp/2.0/themes

```

Gimp City Nights
```
sudo tar xfC gimp-city-nights.tar.gz /usr/share/gimp/2.0/themes

```

Gimp City Black
```
sudo tar xfC gimp-city-black.tar.gz /usr/share/gimp/2.0/themes

```

There is also a matching wallpaper [here](https://gitlab.com/perelax/minimalist-stag-wallpaper), and a matching Android Studio theme [here](https://gitlab.com/perelax/studio-city).
  


## Support
  
Want to say thank you?  
 
[![Buy Me A Coffee](https://cdn.buymeacoffee.com/buttons/default-orange.png){width=120}](https://www.buymeacoffee.com/perelax)

## Authors
Perelax

## License
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

See <https://www.gnu.org/licenses/> for a copy of the GNU General Public License.
